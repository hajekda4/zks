package eshop.tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Open;
import net.thucydides.core.annotations.Step;
import eshop.pageObjects.EshopHomePage;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class StartWith implements Task {

    EshopHomePage eshopHomePage;

    public static StartWith onAHomepage() {
        return instrumented(StartWith.class);
    }

    @Override
    @Step("{0} starts on the homepage with the main menu")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
            Open.browserOn().the(eshopHomePage)
        );
    }
}