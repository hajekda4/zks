package eshop.tasks;

import net.serenitybdd.core.steps.Instrumented;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.JavaScriptClick;
import net.thucydides.core.annotations.Step;

import static eshop.pageObjects.EshopHomePage.FIRST_PRODUCT;

public class AddProductToCart implements Task {

    public static AddProductToCart called() {
        return Instrumented.instanceOf(AddProductToCart.class).withProperties();
    }

    @Step("{0} adds first product to cart")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
            JavaScriptClick.on(FIRST_PRODUCT)
        );
    }
}
