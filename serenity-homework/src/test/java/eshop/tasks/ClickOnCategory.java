package eshop.tasks;

import net.serenitybdd.core.steps.Instrumented;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.*;
import net.thucydides.core.annotations.Step;
import net.serenitybdd.screenplay.targets.Target;

import static eshop.pageObjects.EshopHomePage.HOMEPAGE_LINK;
import static eshop.pageObjects.EshopHomePage.CATEGORY_BUTTON_CASUAL_DRESSES;
import static eshop.pageObjects.EshopHomePage.CATEGORY_BUTTON_SUMMER_DRESSES;

public class ClickOnCategory implements Task {

    String category;

    public ClickOnCategory(String category) {
        this.category = category;
    }

    public static ClickOnCategory called(String category) {
        return Instrumented.instanceOf(ClickOnCategory.class).withProperties(category);
    }


    @Step("{0} clicks on the category button")
    public <T extends Actor> void performAs(T actor) {

        final Target tar;
        if (category.equals("CASUAL DRESSES")) {
            tar = CATEGORY_BUTTON_CASUAL_DRESSES;
        } else if (category.equals("SUMMER DRESSES")) {
            tar = CATEGORY_BUTTON_SUMMER_DRESSES;
        } else {
            tar = HOMEPAGE_LINK;
        }

        actor.attemptsTo(
            JavaScriptClick.on(tar)
        );
    }
}
