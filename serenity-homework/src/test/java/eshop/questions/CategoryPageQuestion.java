package eshop.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

import java.util.List;

import static eshop.pageObjects.EshopHomePage.CATEGORY_NAME_INSIDE_HEADING;

public class CategoryPageQuestion implements Question<List<String>> {

    public static Question<List<String>> theCategoryHeading() {
        return new CategoryPageQuestion();
    }

    @Override
    public List<String> answeredBy(Actor actor) {
        return Text.of(CATEGORY_NAME_INSIDE_HEADING).viewedBy(actor).asList();
    }
}