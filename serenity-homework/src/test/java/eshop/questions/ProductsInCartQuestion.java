package eshop.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

import java.util.List;

import static eshop.pageObjects.EshopHomePage.CART_PRODUCTS;

public class ProductsInCartQuestion implements Question<List<String>> {

    public static Question<List<String>> theProductsInCart() {
        return new ProductsInCartQuestion();
    }

    @Override
    public List<String> answeredBy(Actor actor) {
        List<String> l = Text.of(CART_PRODUCTS).viewedBy(actor).asList();
        return l;
    }
}