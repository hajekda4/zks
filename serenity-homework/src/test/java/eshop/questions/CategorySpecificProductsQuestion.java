package eshop.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

import java.util.List;

import static eshop.pageObjects.EshopHomePage.PRODUCTS_OF_LIST;

public class CategorySpecificProductsQuestion implements Question<List<String>> {

    public static Question<List<String>> theDisplayedCategoryProducts() {
        return new CategorySpecificProductsQuestion();
    }

    @Override
    public List<String> answeredBy(Actor actor) {
        return Text.of(PRODUCTS_OF_LIST).viewedBy(actor).asList();
    }
}