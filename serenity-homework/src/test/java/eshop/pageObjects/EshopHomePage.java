package eshop.pageObjects;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("https://e-shop.webowky.cz/")
public class EshopHomePage extends PageObject {

    public static final Target HOMEPAGE_LINK = Target
        .the("Link to homepage")
        .locatedBy("#header_logo a");
    public static final Target CATEGORY_NAME_INSIDE_HEADING = Target
            .the("Name of the category inside of page heading")
            .locatedBy("h1.page-heading span.cat-name");

    public static final Target CATEGORY_BUTTON_CASUAL_DRESSES = Target
        .the("Category button for Casual Dresses")
        .locatedBy(".submenu-container li a[title='Casual Dresses']");
    public static final Target CATEGORY_BUTTON_SUMMER_DRESSES = Target
        .the("Category button for Summer Dresses")
            .locatedBy(".submenu-container li a[title='Summer Dresses']");

    public static final Target SEARCH_QUERY_TOP = Target
        .the("Search input top")
        .locatedBy("#search_query_top");

    public static final Target PRODUCTS_OF_LIST = Target
            .the("List of products")
            .locatedBy("ul#product_list li .product-container");
    public static final Target FIRST_PRODUCT = Target
            .the("List of products")
            .locatedBy("ul#product_list li:first-child .product-container a.ajax_add_to_cart_button");

    public static final Target CART_PRODUCTS = Target
            .the("Products in shopping cart")
            .locatedBy(".cart_block_list .products dt");
    public static final Target CART_FIRST_PRODUCT = Target
            .the("First product in shopping cart")
            .locatedBy(".cart_block_list .products dt.first_item");




}