package eshop;

import cz.cvut.fel.still.sqa.seleniumStarterPack.config.DriverFactory;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actions.Enter;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import eshop.tasks.ClickOnCategory;
import eshop.tasks.StartWith;

import java.io.IOException;

import static net.serenitybdd.screenplay.GivenWhenThen.*;
import static org.hamcrest.Matchers.*;
import static eshop.questions.CategoryPageQuestion.theCategoryHeading;
import static eshop.questions.CategorySpecificProductsQuestion.theDisplayedCategoryProducts;
import static eshop.pageObjects.EshopHomePage.SEARCH_QUERY_TOP;

@RunWith(SerenityRunner.class)
public class ScenarioOne {

    Actor james = Actor.named("James");

    private WebDriver theBrowser;

    @Before
    public void before() throws IOException {
        theBrowser = new DriverFactory().getDriver();
        givenThat(james).can(BrowseTheWeb.with(theBrowser));
    }

    @Test
    public void should_be_able_to_click_on_category_in_menu() {
        String categoryCasualDresses = "CASUAL DRESSES";
        String ShortDresses = "Krátké šaty";

        givenThat(james).wasAbleTo(StartWith.onAHomepage());

        james.attemptsTo(Enter.theValue("Testovaci zapis").into(SEARCH_QUERY_TOP));

        when(james).attemptsTo(ClickOnCategory.called(categoryCasualDresses));
        then(james).should(seeThat(theCategoryHeading(), anything(categoryCasualDresses)));
        then(james).should(seeThat(theDisplayedCategoryProducts(), anything(ShortDresses)));
        then(james).should(seeThat(theDisplayedCategoryProducts(), notNullValue()));
        then(james).should(seeThat(theDisplayedCategoryProducts(), hasSize(1)));
    }

    @After
    public void closeBrowser() {
        theBrowser.close();
    }
}
