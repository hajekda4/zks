package eshop;

import cz.cvut.fel.still.sqa.seleniumStarterPack.config.DriverFactory;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actions.Enter;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import eshop.tasks.AddProductToCart;
import eshop.tasks.ClickOnCategory;
import eshop.tasks.StartWith;

import java.io.IOException;

import static net.serenitybdd.screenplay.GivenWhenThen.*;
import static org.hamcrest.Matchers.*;
import static eshop.pageObjects.EshopHomePage.SEARCH_QUERY_TOP;
import static eshop.questions.CategoryPageQuestion.theCategoryHeading;
import static eshop.questions.ProductsInCartQuestion.theProductsInCart;
import static eshop.questions.CategorySpecificProductsQuestion.theDisplayedCategoryProducts;

@RunWith(SerenityRunner.class)
public class ScenarioTwo {

    Actor james = Actor.named("James");

    private WebDriver theBrowser;

    @Before
    public void before() throws IOException {
        theBrowser = new DriverFactory().getDriver();
        givenThat(james).can(BrowseTheWeb.with(theBrowser));
    }

    @Test
    public void should_be_able_to_select_a_category_and_add_first_item_to_cart() {
        String categorySummerDresses = "SUMMER DRESSES";

        givenThat(james).wasAbleTo(StartWith.onAHomepage());

        when(james).attemptsTo(ClickOnCategory.called(categorySummerDresses));
        then(james).should(seeThat(theCategoryHeading(), anything(categorySummerDresses)));
        then(james).should(seeThat(theDisplayedCategoryProducts(), notNullValue()));

        then(james).should(seeThat(theProductsInCart(), hasSize(0)));
        when(james).attemptsTo(AddProductToCart.called());

        james.attemptsTo(Enter.theValue("Simulate time").into(SEARCH_QUERY_TOP));
        james.attemptsTo(Enter.theValue("Simulate time2").into(SEARCH_QUERY_TOP));
        james.attemptsTo(Enter.theValue("Simulate time3").into(SEARCH_QUERY_TOP));
        james.attemptsTo(Enter.theValue("Simulate time4").into(SEARCH_QUERY_TOP));

        then(james).should(seeThat(theProductsInCart(), hasSize(1)));
    }

    @After
    public void closeBrowser() {
        theBrowser.close();
    }
}
